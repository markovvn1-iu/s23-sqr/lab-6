import struct
import pytest
from bonus_system import calculateBonuses

def add_to_float(v: float, a: int) -> float:
    res = struct.unpack('d', struct.pack('<Q', struct.unpack('<Q', struct.pack('d', v))[0] + a))[0]
    assert abs(res - v) < 0.001
    return res


@pytest.mark.parametrize('program,multiplier', [
    ("Standard", 0.5),
    ("Premium", 0.1),
    ("Diamond", 0.2),
    ("Unknown", 0),
])
@pytest.mark.parametrize('amount,bonus', [
    (5000, 1),
    (add_to_float(10000, -1), 1),
    (10000, 1.5),
    (25000, 1.5),
    (add_to_float(50000, -1), 1.5),
    (50000, 2),
    (75000, 2),
    (add_to_float(100000, -1), 2),
    (100000, 2.5),
    (200000, 2.5),
])
def test_normal(program: str, amount: float, multiplier: float, bonus: float) -> None:
    assert calculateBonuses(program, amount) == pytest.approx(multiplier * bonus)


@pytest.mark.parametrize('program,amount,result', [
    ("", 5000, 0),
    (None, 5000, 0),
    (-8, 5000, 0),
    (-8, -5000, 0),
    ("Standard", -5000, 0.5),
])
def test_incorrect(program: str, amount: float, result: float) -> None:
    assert calculateBonuses(program, amount) == pytest.approx(result)

    